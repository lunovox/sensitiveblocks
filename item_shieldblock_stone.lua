minetest.register_node("sensitiveblocks:shieldblock_stone", {
	description = "Pedra Escudo",
	tiles = {"default_stone.png"},
	inventory_image = minetest.inventorycube("default_stone.png^text_shieldblock.png"),
	sounds = default.node_sound_stone_defaults(),
	drop = 'sensitiveblocks:shieldblock_stone',
	groups = {cracky=3},
	walkable = false,
	after_place_node = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name() or "")
		--meta:set_string("infotext", "Ampola Vazia")
	end,
})

minetest.register_node("sensitiveblocks:shieldblock_stone_tangible", {
	description = "Pedra Escudo",
	tiles = {"default_stone.png^text_shieldblock.png"},
	inventory_image = minetest.inventorycube("default_stone.png^text_shieldblock.png"),
	sounds = default.node_sound_stone_defaults(),
	drop = 'sensitiveblocks:shieldblock_stone',
	--drawtype = "glasslike",
	--paramtype = "light",
	--paramtype2 = "flowingliquid",
	--sunlight_propagates = true,
	--alpha = WATER_ALPHA,
	groups = {cracky=3, stone=1, not_in_creative_inventory=1},
})

minetest.register_craft({
	output = 'sensitiveblocks:shieldblock_stone',
	recipe = {
		{"default:stone"	,"default:paper"						,"default:stone"},
		{"default:paper"	,sensitiveblocks.sensitive_item	,"default:paper"},
		{"default:stone"	,"default:paper"						,"default:stone"},
	}
})

minetest.register_alias("shieldblockstone"	,"sensitiveblocks:shieldblock_stone")
minetest.register_alias("pedraescudo"			,"sensitiveblocks:shieldblock_stone")
