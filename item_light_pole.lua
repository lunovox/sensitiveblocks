sensitiveblocks.isActivatePole = function(pos)
	local overpos = {x=(pos.x), y=(pos.y+1), z=(pos.z)}
	local luz = minetest.get_node_light(overpos)
	local hora = minetest.get_timeofday()*24
	-- [Luz acima de 7] = com sol | só o sol tem [luz = 15]
	local isOn = false
	if hora < 5.25 or hora > 19 --[[or luz <= 7--]] then --Retirar a consulta de luz por atrapalha quando tem varias luzes proximas.
		isOn = true
	else
		isOn = false
	end
	return isOn
end

sensitiveblocks.print_pole = function(playername, pos)
	local overpos = {x=(pos.x), y=(pos.y+1), z=(pos.z)}
	local luz = minetest.get_node_light(overpos)
	local hora = minetest.get_timeofday()*24
	local isOn = sensitiveblocks.isActivatePole(pos)
	if minetest.get_player_privs(playername).server then
		minetest.chat_send_player(playername, "[print_pole] Hora = "..hora.." | Luz = "..luz.." | isOn = "..dump(isOn))      
	elseif isOn then
		minetest.chat_send_player(playername, "[light_pole] Ativado!")      
	else
		minetest.chat_send_player(playername, "[light_pole] Desativado!")      
	end
end

minetest.register_node("sensitiveblocks:light_pole", {
	description = "Lampada de Poste",
	
	drawtype = "glasslike",
	tiles = {"tex_light_off.png^tex_light_pole.png"},
	inventory_image = minetest.inventorycube("tex_light_on.png^tex_light_pole.png"),
	paramtype = "light",
	is_ground_content = false,
	groups = {cracky=3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_glass_defaults(),
	drop = 'sensitiveblocks:light_pole',
	--[[
	after_place_node = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name() or "")
		--meta:set_string("infotext", "Ampola Vazia")
	end,
	--]]
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		local playername = player:get_player_name()
		sensitiveblocks.print_pole(playername, pos)
	end,
})

minetest.register_node("sensitiveblocks:light_pole_on", {
	description = "Lampada de Poste",
	drawtype = "glasslike",
	tiles = {"tex_light_on.png^tex_light_pole.png"},
	--inventory_image = minetest.inventorycube("tex_light_on.png^tex_eye.png"),
	paramtype = "light",
	sunlight_propagates = true,
	light_source = LIGHT_MAX,
	is_ground_content = false,
	groups = {cracky=3,oddly_breakable_by_hand=3, not_in_creative_inventory=1},
	sounds = default.node_sound_glass_defaults(),
	drop = 'sensitiveblocks:light_pole',
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		local playername = player:get_player_name()
		sensitiveblocks.print_pole(playername, pos)
	end,
})


minetest.register_craft({
	output = 'sensitiveblocks:light_pole',
	recipe = {
		{"default:obsidian_glass"	,"default:obsidian_glass"			,"default:obsidian_glass"},
		{"default:obsidian_glass"	,sensitiveblocks.sensitive_item	,"default:obsidian_glass"},
		{"default:obsidian_glass"	,"default:obsidian_glass"			,"default:obsidian_glass"},
	}
})

minetest.register_alias("light_pole"		,"sensitiveblocks:light_pole")
minetest.register_alias("lampadadeposte"	,"sensitiveblocks:light_pole")
