minetest.register_node("sensitiveblocks:ghostblock_stone", {
	description = "Pedra Fantasma",
	tiles = {"default_stone.png"},
	inventory_image = minetest.inventorycube("default_stone.png^text_ghostblock.png"),
	sounds = default.node_sound_stone_defaults(),
	drop = 'sensitiveblocks:ghostblock_stone',
	groups = {cracky=3},
	after_place_node = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name() or "")
		--meta:set_string("infotext", "Ampola Vazia")
	end,
})

minetest.register_node("sensitiveblocks:ghostblock_stone_intangible", {
	description = "Pedra Fantasma",
	tiles = {"default_stone.png^text_ghostblock.png"},
	inventory_image = minetest.inventorycube("default_stone.png^text_ghostblock.png"),
	sounds = default.node_sound_stone_defaults(),
	drop = 'sensitiveblocks:ghostblock_stone',
	drawtype = "glasslike",
	paramtype = "light",
	paramtype2 = "flowingliquid",
	sunlight_propagates = true,
	walkable = false,
	alpha = WATER_ALPHA,
	groups = {cracky=3, stone=1, not_in_creative_inventory=1},
})

minetest.register_craft({
	output = 'sensitiveblocks:ghostblock_stone',
	recipe = {
		{"default:stone"				,"default:obsidian_glass"			,"default:stone"},
		{"default:obsidian_glass"	,sensitiveblocks.sensitive_item	,"default:obsidian_glass"},
		{"default:stone"				,"default:obsidian_glass"			,"default:stone"},
	}
})

minetest.register_alias("ghostblockstone"	,"sensitiveblocks:ghostblock_stone")
minetest.register_alias("pedrafantasma"	,"sensitiveblocks:ghostblock_stone")
