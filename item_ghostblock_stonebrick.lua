minetest.register_node("sensitiveblocks:ghostblock_stonebrick", {
	description = "Alvenaria de Pedra Fantasma",
	tiles = {"default_stone_brick.png"},
	inventory_image = minetest.inventorycube("default_stone_brick.png^text_ghostblock.png"),
	sounds = default.node_sound_stone_defaults(),
	drop = 'sensitiveblocks:ghostblock_stonebrick',
	groups = {cracky=3},
	after_place_node = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name() or "")
		--meta:set_string("infotext", "Ampola Vazia")
	end,
})

minetest.register_node("sensitiveblocks:ghostblock_stonebrick_intangible", {
	description = "Alvenaria de Pedra Fantasma",
	tiles = {"default_stone_brick.png^text_ghostblock.png"},
	inventory_image = minetest.inventorycube("default_stone_brick.png^text_ghostblock.png"),
	sounds = default.node_sound_stone_defaults(),
	drop = 'sensitiveblocks:ghostblock_stonebrick',
	drawtype = "glasslike",
	paramtype = "light",
	paramtype2 = "flowingliquid",
	sunlight_propagates = true,
	walkable = false,
	alpha = WATER_ALPHA,
	groups = {cracky=3, stone=1, not_in_creative_inventory=1},
})

minetest.register_craft({
	output = 'sensitiveblocks:ghostblock_stonebrick',
	recipe = {
		{"default:stonebrick"		,"default:obsidian_glass"			,"default:stonebrick"},
		{"default:obsidian_glass"	,sensitiveblocks.sensitive_item	,"default:obsidian_glass"},
		{"default:stonebrick"		,"default:obsidian_glass"			,"default:stonebrick"},
	}
})

minetest.register_alias("ghostblockstonebrick"			,"sensitiveblocks:ghostblock_stonebrick")
minetest.register_alias("alvenariadepedrafantasma"		,"sensitiveblocks:ghostblock_stonebrick")
