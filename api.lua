sensitiveblocks.nodenames = {
	"sensitiveblocks:shieldblock_stone"			, "sensitiveblocks:shieldblock_stone_tangible", 
	"sensitiveblocks:ghostblock_stone"			, "sensitiveblocks:ghostblock_stone_intangible", 
	"sensitiveblocks:ghostblock_stonebrick"	, "sensitiveblocks:ghostblock_stonebrick_intangible",
	"sensitiveblocks:lightsensitive"				, "sensitiveblocks:lightsensitive_on",
	"sensitiveblocks:light_pole"					, "sensitiveblocks:light_pole_on",
}

minetest.register_abm({
	nodenames = sensitiveblocks.nodenames,
	interval = 1.0,
	chance = 1,
	action = function(pos)
		--print("minetest.env:get_node(pos).name="..minetest.env:get_node(pos).name)
		local meta = minetest.get_meta(pos)
		local ownername = meta:get_string("owner")
		if ownername~=nil and type(ownername)=="string" and ownername~="" then
			local ownerplayer = minetest.env:get_player_by_name(ownername)
			if ownerplayer~=nil and ownerplayer:is_player() then
				local b = pos
				local p = ownerplayer:getpos()
				local dist = ((p.x-b.x)^2 + (p.y-b.y)^2 + (p.z-b.z)^2)^0.5
				--print("dist="..dist)
				if dist <= sensitiveblocks.sensitive_distance then
					if minetest.env:get_node(pos).name == "sensitiveblocks:shieldblock_stone" then
						minetest.env:add_node(pos, {name="sensitiveblocks:shieldblock_stone_tangible"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
					if minetest.env:get_node(pos).name == "sensitiveblocks:ghostblock_stone" then
						minetest.env:add_node(pos, {name="sensitiveblocks:ghostblock_stone_intangible"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
					if minetest.env:get_node(pos).name == "sensitiveblocks:ghostblock_stonebrick" then
						minetest.env:add_node(pos, {name="sensitiveblocks:ghostblock_stonebrick_intangible"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
					if minetest.env:get_node(pos).name == "sensitiveblocks:lightsensitive" then
						minetest.env:add_node(pos, {name="sensitiveblocks:lightsensitive_on"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
				else
					if minetest.env:get_node(pos).name == "sensitiveblocks:shieldblock_stone_tangible" then
						minetest.env:add_node(pos, {name="sensitiveblocks:shieldblock_stone"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
					if minetest.env:get_node(pos).name == "sensitiveblocks:ghostblock_stone_intangible" then
						minetest.env:add_node(pos, {name="sensitiveblocks:ghostblock_stone"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
					if minetest.env:get_node(pos).name == "sensitiveblocks:ghostblock_stonebrick_intangible" then
						minetest.env:add_node(pos, {name="sensitiveblocks:ghostblock_stonebrick"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
					if minetest.env:get_node(pos).name == "sensitiveblocks:lightsensitive_on" then
						minetest.env:add_node(pos, {name="sensitiveblocks:lightsensitive"})
						local meta = minetest.get_meta(pos)
						meta:set_string("owner", ownername)
					end
				end
			else --else if ownerplayer~=nil and ownerplayer:is_player() then
				if minetest.env:get_node(pos).name == "sensitiveblocks:shieldblock_stone_tangible" then
					minetest.env:add_node(pos, {name="sensitiveblocks:shieldblock_stone"})
					local meta = minetest.get_meta(pos)
					meta:set_string("owner", ownername)
				end
				if minetest.env:get_node(pos).name == "sensitiveblocks:ghostblock_stonebrick_intangible" then
					minetest.env:add_node(pos, {name="sensitiveblocks:ghostblock_stonebrick"})
					local meta = minetest.get_meta(pos)
					meta:set_string("owner", ownername)
				end
				if minetest.env:get_node(pos).name == "sensitiveblocks:ghostblock_stonebrick_intangible" then
					minetest.env:add_node(pos, {name="sensitiveblocks:ghostblock_stonebrick"})
					local meta = minetest.get_meta(pos)
					meta:set_string("owner", ownername)
				end
				if minetest.env:get_node(pos).name == "sensitiveblocks:lightsensitive_on" then
					minetest.env:add_node(pos, {name="sensitiveblocks:lightsensitive"})
					local meta = minetest.get_meta(pos)
					meta:set_string("owner", ownername)
				end
			end
		else
			if sensitiveblocks.isActivatePole(pos) then
				if minetest.env:get_node(pos).name == "sensitiveblocks:light_pole" then
					minetest.env:add_node(pos, {name="sensitiveblocks:light_pole_on"})
				end
			else
				if minetest.env:get_node(pos).name == "sensitiveblocks:light_pole_on" then
					minetest.env:add_node(pos, {name="sensitiveblocks:light_pole"})
				end
			end
			
		end
	end,
})
