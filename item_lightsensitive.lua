minetest.register_node("sensitiveblocks:lightsensitive", {
	description = "Lampada de Presenca",
	
	drawtype = "glasslike",
	tiles = {"tex_light_off.png"},
	inventory_image = minetest.inventorycube("tex_light_on.png^tex_eye.png"),
	paramtype = "light",
	is_ground_content = false,
	groups = {cracky=3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_glass_defaults(),
	drop = 'sensitiveblocks:lightsensitive',

	after_place_node = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name() or "")
		--meta:set_string("infotext", "Ampola Vazia")
	end,
})

minetest.register_node("sensitiveblocks:lightsensitive_on", {
	description = "Lampada de Presenca",
	drawtype = "glasslike",
	tiles = {"tex_light_on.png"},
	--inventory_image = minetest.inventorycube("tex_light_on.png^tex_eye.png"),
	paramtype = "light",
	sunlight_propagates = true,
	light_source = LIGHT_MAX,
	is_ground_content = false,
	groups = {cracky=3,oddly_breakable_by_hand=3, not_in_creative_inventory=1},
	sounds = default.node_sound_glass_defaults(),
	drop = 'sensitiveblocks:lightsensitive',
})


minetest.register_craft({
	output = 'sensitiveblocks:lightsensitive',
	recipe = {
		{"default:glass"				,"default:obsidian_glass"			,"default:glass"},
		{"default:obsidian_glass"	,sensitiveblocks.sensitive_item	,"default:obsidian_glass"},
		{"default:glass"				,"default:obsidian_glass"			,"default:glass"},
	}
})

minetest.register_alias("lightsensitive"		,"sensitiveblocks:lightsensitive")
minetest.register_alias("lampadadepresenca"	,"sensitiveblocks:lightsensitive")
