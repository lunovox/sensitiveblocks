local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

sensitiveblocks = {
	sensitive_item = "default:mese_crystal",
	sensitive_distance = 6
}

dofile(modpath.."/api.lua")

dofile(modpath.."/item_ghostblock_stone.lua")
dofile(modpath.."/item_ghostblock_stonebrick.lua")
dofile(modpath.."/item_lightsensitive.lua")
dofile(modpath.."/item_light_pole.lua")
dofile(modpath.."/item_shieldblock_stone.lua")

minetest.log('action',"["..modname:upper().."] Carregado!")
